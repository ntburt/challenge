package challenge.processor;

import java.util.List;
import java.util.stream.Collectors;

import challenge.model.Device;
import challenge.model.Result;

/**
 * Processes collected Device data
 *
 */
public class DevicesOfInterestProcessor {
	
	private double topLeftLon;
	private double topLeftLat;
	private double bottomRightLat;
	private double bottomRightLon;

	/**
	 * Constructs a Processor with area of interest rectangle
	 * @param topLeftLat
	 * @param topLeftLon
	 * @param bottomRightLat
	 * @param bottomRightLon
	 */
	public DevicesOfInterestProcessor(double topLeftLat, double topLeftLon, double bottomRightLat, double bottomRightLon) {
		this.topLeftLat = topLeftLat;
		this.topLeftLon = topLeftLon;
		this.bottomRightLat = bottomRightLat;
		this.bottomRightLon = bottomRightLon;
	}

	/**
	 * 
	 * @param devices list of devices to process
	 * @return Result object with statistics of devices processed, including devices ids in area of interest
	 */
	public Result process(List<Device> devices) {
		long uniqueUuids = devices.stream()
				.map(Device::getUuid)
				.distinct()
				.count();
		
		List<String> geoListFiltered = devices.stream()
				.filter((d) -> {
					return d.getLon() < bottomRightLon && d.getLat() > bottomRightLat && d.getLon() > topLeftLon && d.getLat() < topLeftLat;
				})
				.map(Device::getUuid)
				.collect(Collectors.toList());
		
		return new Result(devices.size(), uniqueUuids, geoListFiltered, devices.get(0), devices.get(devices.size()-1));
	}
}
