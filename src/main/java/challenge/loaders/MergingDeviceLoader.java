package challenge.loaders;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import challenge.loaders.DataLoaderFactory;
import challenge.loaders.IDataLoader;
import challenge.model.Device;

/**
 * 
 * Takes in files of potentially different formats but same type and returns 
 * collected set of data
 * Optionally deduplicates using Set vs List implementation
 *
 */
public class MergingDeviceLoader implements IDataLoader {

	boolean deduplicate = true;
	private List<File> files;
	
	public MergingDeviceLoader(List<File> files, boolean dedup) {
		this.files = files;
		this.deduplicate = dedup;
	}

	@Override
	public Collection<Device> getData() {
		
		List<IDataLoader> loaders = new ArrayList<>(files.size());
		files.forEach((f) -> loaders.add(DataLoaderFactory.create(f)));
		
		Collection<Device> devices = deduplicate ? new HashSet<>() : new ArrayList<>();
		loaders.forEach((l) -> {
			Collection<Device> data = l.getData();
			System.out.println(String.format("Loaded %d entries for %s", data.size(), l.getClass()));
			devices.addAll(l.getData());
		});
		
		return devices;
	}
	
	public static class Builder {
		List<File> files;
		private boolean dedup;
		public Builder addFiles(List<File> files) {
			this.files = files;
			return this;
		}
		
		public Builder addFile(File file) {
			if(files == null)
				files = new ArrayList<>();
			files.add(file);
			return this;
		}
		
		public Builder dedup(boolean shouldDedup) {
			this.dedup = shouldDedup;
			return this;
		}
		
		public MergingDeviceLoader build() {
			return new MergingDeviceLoader(files, dedup);
		}
	}
}
