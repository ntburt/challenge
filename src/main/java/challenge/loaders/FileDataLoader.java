package challenge.loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


public abstract class FileDataLoader implements IDataLoader {

	File toLoad;
	
	public FileDataLoader(File inFile) {
		this.toLoad = inFile;
	}
	
	protected InputStream getDataStream() throws FileNotFoundException {
		InputStream in = new FileInputStream(toLoad);
		return in;
	}
	


}
