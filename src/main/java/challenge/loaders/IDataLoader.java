package challenge.loaders;

import java.util.Collection;

import challenge.model.Device;

public interface IDataLoader {
	Collection<Device> getData();
}
