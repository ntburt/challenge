package challenge.loaders;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import challenge.model.Device;
import challenge.model.Devices;

public class JsonDeviceLoader extends FileDataLoader implements IDataLoader {

	ObjectMapper mapper = new ObjectMapper();
	public JsonDeviceLoader(File inFile) {
		super(inFile);
	}

	@Override
	public List<Device> getData()  {
		Devices devices = null;
		
		try {
			devices = mapper.readValue(getDataStream(), Devices.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return devices == null ? Collections.emptyList() : devices.getDevices();
	}

}
