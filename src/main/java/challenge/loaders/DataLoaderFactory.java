package challenge.loaders;

import java.io.File;

public class DataLoaderFactory {
	public static IDataLoader create(File file) {
		if(!file.exists()) {
			throw new IllegalArgumentException("File does not exist");
		}
		String fileName = file.getName();
		String fileType = ""; 
		// just using the extension, could enhance to handle more complex checking
		int i = file.getName().lastIndexOf('.');
		if (i >= 0) {
		    fileType = fileName.substring(i+1);
		}
		
		if("JSON".equalsIgnoreCase(fileType)) {
			return new JsonDeviceLoader(file);
		} else if("CSV".equalsIgnoreCase(fileType)) {
			return new CsvDeviceLoader(file);
		} else {
			throw new IllegalStateException("File type not supported");
		}
	}
}
