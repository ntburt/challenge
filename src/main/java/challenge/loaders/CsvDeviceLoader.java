package challenge.loaders;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.csv.CsvSchema.ColumnType;

import challenge.model.Device;

public class CsvDeviceLoader extends FileDataLoader implements IDataLoader {

	CsvMapper mapper = new CsvMapper();
	
	public CsvDeviceLoader(File inFile) {
		super(inFile);
	}

	@Override
	public List<Device> getData() {
		List<Device> results = null;
		try {
			CsvSchema schema = CsvSchema.builder()
					.addColumn("uuid", ColumnType.STRING)
					.addColumn("lat", ColumnType.NUMBER)
					.addColumn("long", ColumnType.NUMBER)
					.addColumn("timestamp", ColumnType.NUMBER)
					.setSkipFirstDataRow(true)
					.build();
			  MappingIterator<Device> it = mapper.readerFor(Device.class)
					    .with(schema)
					    .readValues(getDataStream());
			  results = new ArrayList<>();
			  while (it.hasNextValue()) {
			    Device d = it.nextValue();
			    results.add(d);
			  }
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results == null ? Collections.emptyList() : results;
	}

}
