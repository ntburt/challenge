package challenge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import challenge.loaders.MergingDeviceLoader;
import challenge.model.Device;
import challenge.model.Result;
import challenge.processor.DevicesOfInterestProcessor;

@SpringBootApplication
public class ChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            // Load data, deduplicate along the way
            MergingDeviceLoader dataLoader = new MergingDeviceLoader.Builder()
            		.addFile(ResourceUtils.getFile("classpath:data.json"))
            		.addFile(ResourceUtils.getFile("classpath:data.csv"))
            		.dedup(true)
             		.build();

            // sort the data
            List<Device> devices = new ArrayList<>(dataLoader.getData());
            devices.sort(Comparator.comparingLong(Device::getTimestamp));
        	
            // process results
            // Longitude: -179.99 Latitude: 89.99, Longitude: 0.00, Latitude: 0.00
        	Result results = new DevicesOfInterestProcessor(89.99, -179.99, 0.0, 0.0).process(devices);
        	//devices.forEach(d -> System.out.println(d));
        	System.out.println(results);
    		
        	// output results
        	ObjectMapper mapper = new ObjectMapper();
    		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
    		try {
    			writer.writeValue(new File("results.json"), results);
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
        };
    }

}
