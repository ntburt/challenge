package challenge.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.time.Instant;
import java.time.ZoneId;

public class Result {
	
	private long numDevices;
	private long uniqueUuids;
	private List<String> devicesOfInterest;
	private Device oldestDevice;
	private Device newestDevice;

	public Result(long numDevices, long numUniqueUuids, List<String> geoDevices,  Device oldestDevice, Device newestDevice) {
		super();
		this.numDevices = numDevices;
		this.uniqueUuids = numUniqueUuids;
		this.devicesOfInterest = geoDevices == null ? Collections.emptyList() : geoDevices;
		this.oldestDevice = oldestDevice;
		this.newestDevice = newestDevice;
	}
	
	public long getNumDevices() {
		return numDevices;
	}
	public long getUniqueUuids() {
		return uniqueUuids;
	}
	public int getNumUuidsOfInterest() {
		return devicesOfInterest.size();
	}

	public List<String> getUuidsOfInterest() {
		return devicesOfInterest;
	}
	public Map<String, String> getOldestDevice() {
		return deviceDetails(oldestDevice);
	}
	public Map<String, String> getNewestDevice() {
		return deviceDetails(newestDevice);
	}
	
	private static Map<String,String> deviceDetails(Device d) {
		Map<String, String> details = new HashMap<>();
		details.put("uuid", d.getUuid());
		details.put("dateTime", Instant.ofEpochMilli(d.getTimestamp()*1000).atZone(ZoneId.systemDefault()).toLocalDateTime().toString());
		return details;
	}

	@Override
	public String toString() {

		return "Result [numDevices=" + numDevices + ", uniqueDevices=" + uniqueUuids + ", numInGeoWindow="
				+ getNumUuidsOfInterest() 
				+ ", oldestDevice=" + oldestDevice.getUuid() + " " +  Instant.ofEpochMilli(oldestDevice.getTimestamp()*1000).atZone(ZoneId.systemDefault()).toLocalDateTime()
				+ ", newestDevice=" + newestDevice.getUuid() + " " + Instant.ofEpochMilli(newestDevice.getTimestamp()*1000).atZone(ZoneId.systemDefault()).toLocalDateTime() 
				+"]";
	}
	
	
}
